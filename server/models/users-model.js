const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'auction'
});

const usersModel = {
    getUser: function (callback) {
        return connection.query("SELECT * FROM users", callback);
    },
    getUserById: function(id, callback) {
        return connection.query("SELECT * FROM users WHERE id = ?", id, callback);
    },
    loggedUser: function (login, pass, callback) {
        return connection.query("SELECT * FROM users WHERE login = ? AND password = ?", [login, pass], callback);
    },
    searchUser: function (login, callback) {
        return connection.query("SELECT * FROM users WHERE login = ?", login, callback);
    },
    createUser: function (userData, callback) {
        return connection.query("INSERT INTO users SET ?", userData, callback);
    },
    checkToken: function (token, callback) {
        return connection.query('SELECT id FROM users WHERE jwt_token = ?', token, callback);
    }
};
 
module.exports = usersModel;