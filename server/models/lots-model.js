let mysql = require('mysql');
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'auction'
});


const lotsModel = {
    getAllLots: function (callback) {
        return connection.query("SELECT * FROM lots", callback);
    },
    getOneLot: function (id, callback) {
        return connection.query("SELECT * FROM lots WHERE id = ?", id, callback);
    },
    getUsersLots: function (id, callback) {
        return connection.query("SELECT * FROM lots WHERE author = ?", id, callback)
    },
    createNewLot: function (data, callback) {
        return connection.query("INSERT INTO lots SET ?", data, callback);
    },
    deleteOne: function (author_id, lot_id, callback) {
        return connection.query("DELETE FROM lots WHERE author = ? AND id = ?", [author_id, lot_id], callback);
    },
    getUserID: function (user_id, callback) {
        return connection.query("SELECT * FROM bids WHERE lot_id = ? ORDER BY id DESC LIMIT 1", user_id, callback);
    } 
};
module.exports = lotsModel;