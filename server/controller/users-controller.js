const fromUsers = require('../models/users-model');
const jwt = require('jsonwebtoken');

const userController = {
    getUser: function (req, res) {
        return fromUsers.getUser((err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Database").end();
            }

            res.json(result);
        })
    },
    getUserById: function (req, res) {
        return fromUsers.getUserById(req.params.id ,(err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Database").end();
            }

            res.json(result);
        })
    },
    loggedUser: function (req, res) {
        let usersLoginData = {
            login: req.body.login,
            password: req.body.password
        };
        return fromUsers.loggedUser(usersLoginData.login, usersLoginData.password, (err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Database")
            }
            console.log(result);
            if (result.length === 0) {
                res.status(401).send({message: "Enter correct login and/or password"});
            } else {
                res.json(result);
            }
        })
    },
    createUser: function (req, res) {
        let userLoginData = {
            login: req.body.login,
            password: req.body.password,
            name: req.body.name,
            surname: req.body.surname,
            email: req.body.email,
            jwt_token: ''
        };
        return fromUsers.searchUser(userLoginData.login, (err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Database").end();
            }
            console.log(result);

            userLoginData.jwt_token = jwt.sign({login: userLoginData.login, email: userLoginData.email}, "react-jwt-token-auth");

            if (result.length === 0) {
                fromUsers.createUser(userLoginData, (err, result) => {
                    console.log(result);
                    res.json({message: "New User is created"}).end();
                })
            } else {
                res.json({message: "User with this login is exist"}).end();
            }

        })
    },
    checkToken: function (req, res) {
        return fromUsers.checkToken(req.params.token, (err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).end("Error with Database");
            }

            if (result.length === 0) {
                res.send("User does not exist").end();
            }

            res.json(result);
        })
    }
};

module.exports = userController;