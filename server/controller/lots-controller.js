const fromLots = require('../models/lots-model');
const lotsController = {
    getLots: function (req, res) {
        return fromLots.getAllLots((err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Database").end();
            }

            res.json(result);
        })
    },
    getOneLot: function (req, res) {
        return fromLots.getOneLot(req.params.id, (err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Database").end();
            }

            res.json(result);
        })
    },
    getUsersLots: function (req, res) {
        return fromLots.getUsersLots(req.params.id, (err, result) =>{
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Database").end();
            }

            res.json(result);
        })
    },
    deleteOne: function (req, res) {
        console.log(req.params.author, req.params.id);
        return fromLots.deleteOne(req.params.author, req.params.id, (err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Datebase").end();
            }
            res.json(result);
        })
    },
    getUserID: function(req, res) {
        console.log(req.params.id);
        return fromLots.getUserID(req.params.id, (err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Datebase").end();
            }

            res.json(result);
        })
    }
};

module.exports = lotsController;