const express = require('express');
const router = express.Router();

// Подключаем контроллер Users
const usersController = require('../controller/users-controller');

router.get('/users/get-user', usersController.getUser);
router.get('/users/get-user/:id', usersController.getUserById);
router.get('/users/check-token/:token', usersController.checkToken);
router.post('/users/login-user', usersController.loggedUser);
router.post('/users/create-new-user', usersController.createUser);

module.exports = router;