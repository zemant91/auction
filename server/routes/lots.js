const express = require('express');
const router = express.Router();

// Подключаем контроллер Lots
const lotsController = require('../controller/lots-controller');

router.get('/lots/get-all', lotsController.getLots);
router.get('/lots/get-one/:id', lotsController.getOneLot);
router.get('/lots/get-user-lots/:id', lotsController.getUsersLots);
router.delete('/lots/delete-one/:author/:id', lotsController.deleteOne);
router.get('/lots/get-last-user-bid/:id', lotsController.getUserID);

module.exports = router;