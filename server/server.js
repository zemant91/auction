const express = require('express');
const app = express();
const server = require('http').Server(app);
const axios = require('axios');
const bodyParser = require('body-parser');
const port = 3001;
const cors = require('cors');
const users = require('./routes/users');
const lots = require('./routes/lots');
const mime = require('mime-types');
const crypto = require('crypto');
const multer  = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            cb(null, raw.toString('hex') + Date.now() + '.' + mime.extension(file.mimetype));
        });
    }
});
const upload = multer({ storage: storage });

// Подключение к базе Mysql
const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password : '',
    database : 'auction'
});

connection.connect(function(err){
    if(err) {
        console.log('error connecting: ' + err.stack);
        return;
    }

    console.log('connected as id ' + connection.threadId);
});

// Подключение промежуточного ПО
app.use(express.static(__dirname + '/uploads'));
app.use(cors());
app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Подклбчени роутов для работы с Users
app.use('/api', users);
app.use('/api', lots);
app.put('/api/files/avatar-upload/:id', upload.single('file'), (req, res) => {
    const imageData = {avatar: req.file.filename, id: req.params.id};
    console.log(req.file);
    console.log(imageData.avatar);
    connection.query('UPDATE `users` SET `users`.`avatar` = ? WHERE id = ?', [imageData.avatar, imageData.id],(err, result) => {
        if (err) {
            console.log(err);
            res.sendStatus(500).send("Error with Database").end();
        }

        res.json(result);
    })
});


const fromLots = require('./models/lots-model')
app.post('/api/lots/add-new', (req, res) => {
        const PostData = {
            author: req.body.author,
            title: req.body.title,
            startPrice: req.body.startPrice,
            description: req.body.description,
            end: req.body.end 
        }
        console.log(PostData);
        return fromLots.createNewLot(PostData, (err, result) => {
            if (err) {
                console.log(err);
                res.sendStatus(500).send("Error with Datebase").end();
            }
            console.log(req.body); 
            res.json(result);
        })
});

// Прослушаем сервер на заданном порту
server.listen(port,() => {
    console.log(`Server is listening on port:${port}`)
});

const io = require('socket.io')(server);

io.on('connection', (socket) => {
    let oldRoom;
    console.log('User connected ' + socket.id);
    socket.on('disconnect', () => {
        console.log('User disconnected ' + socket.id);
    })

    socket.on('old_room', (room) => {
        oldRoom = room;
        console.log(oldRoom);
    })

    socket.on('into_room', (room) => {
        console.log("Old room was " + oldRoom);
        socket.leave(oldRoom);
        console.log("User leave room #" + oldRoom);
        socket.join(room);
        socket.room = room;

        socket.on('bid', (data) => {
                const data_bid = {
                user_id: data.user,
                lot_id: data.id_lot,
                newPrice: data.bid
            }

            connection.query("INSERT INTO bids SET ?", data_bid, (req, res) => {
                connection.query("SELECT * FROM lots WHERE id = ?", data_bid.lot_id, (err, result) => {
                    let newPrice = parseInt(data_bid.newPrice) + parseInt(result[0].startPrice);
                    connection.query("UPDATE lots SET startPrice = ? WHERE id = ?", [newPrice, data_bid.lot_id], (err, result) => {
                        connection.query("SELECT * FROM lots WHERE id = ? ORDER BY id DESC LIMIT 1", data_bid.lot_id, (err, res) => {
                            io.sockets.emit('refresh_price', {price: res[0].startPrice, id: data_bid.lot_id});
                            io.sockets.in(socket.room).emit('take_bid', {lastbid: data_bid.newPrice, user: data_bid.user_id, newPrice: res[0].startPrice});
                        })
                    })
                })
            })
        })
    })
})



