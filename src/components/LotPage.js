import React, {Component} from 'react';
import LotsService from "../services/LotsService";
import {Link} from 'react-router-dom';
import CountdownTimer from './CountdownTimer';
const io = require('socket.io-client');
const socket = io();

class LotPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lotInfo: [],
            lastuser: '',
            lastbid: null,
            price: null,
            bidValue: 0,
            newPrice: null
        };

        this.addLotsService = new LotsService();
        this.HandlerBidButton = this.HandlerBidButton.bind(this);
        this.setBidValue = this.setBidValue.bind(this);
        this.takeBidAndUser = this.takeBidAndUser.bind(this);
    }

    componentDidMount() {
        this.IntoRoom(this.props.match.params.id)
        
        this.addLotsService.GetOneLot(this.props.match.params.id, (res) => {
            this.setState({ lotInfo: res.data });
        })

        this.addLotsService.GetUserID(this.props.match.params.id, (res) => {
            //console.log(res);
            this.setState({lastuser: res.data[0].user_id, lastbid: res.data[0].newPrice});
        })
    }

    componentWillUnmount() {
        socket.emit('old_room', this.props.match.params.id);
    }

    IntoRoom(room) {
        console.log("User in room #" + room);
        socket.emit('into_room', room);
    }

    HandlerBidButton () {
        socket.emit('bid', {
            user: localStorage.getItem("id_user"), 
            bid: this.state.bidValue, 
            id_lot: this.props.match.params.id
        });
    }

    setBidValue(e) {
        this.setState({
            bidValue: e.target.value
        })
        this.takeBidAndUser();
    }

    takeBidAndUser() {
        socket.on('take_bid', (data) => {
            this.setState({
                lastuser: data.user,
                lastbid: data.lastbid,
                newPrice: data.newPrice
            })
        })
    }

    ShowBidsButton(author) {
        return (
            <div className="col s6">
                <h3>Bids</h3>
                {(localStorage.getItem("id_user") == author) ? 
                this.OwnerCantBids() :
                <div>
                    <input type="number" value={this.state.bidValue} onChange={this.setBidValue}/>
                    <button type="button" onClick={this.HandlerBidButton}>Bid</button>
                </div>
                }
            </div>
        )
    }

    ShowLoginAnchor() {
        return (
            <div>
                You must <Link to="/login">login</Link> to make an offer!
            </div>
        )
    }

    OwnerCantBids() {
        return (
            <h4>You can't bid on your own lot. Be fair user =)</h4>
        )
    }

    render() {


        return (
            <div className="container">
                <div className="row">
                    <h2>Lot Page</h2>
                    {this.state.lotInfo.map((lotInfo, index) => {
                        return (
                            <div key={index}>
                                <div className="col s4">
                                    <img src={lotInfo.image ? lotInfo.image : "http://via.placeholder.com/250x250"} alt=""/>
                                    <br/>Lot index: {lotInfo.id} <br/>
                                    Lot Description <br/>
                                    <p>{lotInfo.description}</p>  
                                </div>
                                <div className="col s4">
                                    {
                                    Date.now() >= Date.parse(lotInfo.end) ? 
                                    <div>
                                        <h2>Auction is end</h2>
                                    </div> :
                                    <div>
                                        <CountdownTimer end={lotInfo.end}/>
                                        <div>
                                            {localStorage.getItem("token") ? this.ShowBidsButton(lotInfo.author) : this.ShowLoginAnchor()}
                                        </div>
                                    </div>
                                    }
                                </div>
                                <div className="col s4">
                                    Last User: 
                                    {this.state.lastuser} <br/>
                                    Last Bid: 
                                    {this.state.lastbid} <br/>
                                    Final Price: <br/>
                                    {this.state.newPrice ? this.state.newPrice : lotInfo.startPrice}
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
        )
    }
}

export default LotPage;