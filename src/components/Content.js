import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import LoginForm from "./LoginForm";
import RegisterForm from "./RegisterForm";
import MainPage from "./MainPage";
import LotsList from "./LotsList";
import UserProfile from "./UserProfile";
import LotPage from "./LotPage";
import AddNewLot from './AddNewLot';
import PageNotFound from "./PageNotFound";

class Content extends Component {
    render() {
        return (
            <Switch>
                <Route exact path='/' component={MainPage}/>
                <Route path='/login' component={LoginForm}/>
                <Route path='/register' component={RegisterForm}/>
                <Route path='/lots' component={LotsList}/>
                <Route exact path='/user-profile' component={UserProfile} />
                <Route path='/user-profile/add-new-lot' component={AddNewLot}/>
                <Route path='/lot/:id' component={LotPage}/>
                <Route path='*' component={PageNotFound}/>
            </Switch>
        )
    }
}

export default Content;