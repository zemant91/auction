import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import AddLotService from '../services/AddLotService';

class AddNewLot extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formValues: {
                author: localStorage.getItem("id_user")
            }
        }

        this.onChangeInput = this.onChangeInput.bind(this);
        this.onButtonClick = this.onButtonClick.bind(this);
        this.RedirectToUserProfile = this.RedirectToUserProfile.bind(this);
        this.addAddLotService = new AddLotService();
    }

    onChangeInput(e) {
        
        let formValues = this.state.formValues;
        formValues[e.target.name] = e.target.value;

        this.setState({formValues});
    }

    onButtonClick() {
        let date = document.getElementsByName('end')[0].value.replace('T', ' ');
        date += ':00';
        if (!this.state.formValues.title) {
            alert("Title is empty");
            return false;
        }

        if (!this.state.formValues.description) {
            alert("Description is empty");
            return false;
        }

        if (!this.state.formValues.startPrice) {
            alert("Price is empty");
            return false;
        }

        if (!this.state.formValues.end) {
            alert("Date is empty");
            return false;
        }

        this.addAddLotService.AddNewLot(this.state.formValues, date, (res) => {
            this.RedirectToUserProfile();
        });
    }

    RedirectToUserProfile() {
        this.setState({redirect: true});
    }

    render() {
        if (this.state.redirect === true) {
            return <Redirect to='/user-profile'/>;
        }
        
        return (
            <div className="container">
                <div className="row">
                    <h2>Add New Lot</h2>
                        Name:<br/>
                        <input type="text" name="title" value={this.state.formValues["title"]} onChange={this.onChangeInput} required={true}/>
                        Description: <br/>
                        <textarea name="description" id="" cols="30" rows="10" value={this.state.formValues["description"]} onChange={this.onChangeInput} required='true'></textarea>
                        Price: <br/>
                        <input type="number" name="startPrice" value={this.state.formValues["startPrice"]} onChange={this.onChangeInput} required='true'/>
                        <input type="datetime-local" name="end" value={this.state.formValues["end"]} onChange={this.onChangeInput} required='true'/>
                        <button type="submit" onClick={this.onButtonClick}>Upload</button>
                </div>
            </div>
        )
    }
}

export default AddNewLot;