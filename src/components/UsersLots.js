import React, {Component} from 'react';
import LotsService from "../services/LotsService";
import {Link} from 'react-router-dom';

class UsersLots extends Component {
    constructor(props) {
        super(props);
        this.state = {
            author: this.props.author,
            lots: []
        };

        this.addLotsService = new LotsService();
        this.DeleteLotHandler = this.DeleteLotHandler.bind(this);
    }

    componentDidMount() {
        this.addLotsService.GetUsersLots(this.state.author, (res) => {
            this.setState({lots: res.data});
        })
    }
    
    DeleteLotHandler(e) {
        e.preventDefault();
        this.addLotsService.DeleteLot(this.state.author, e.target.value, (res) => {
            this.addLotsService.GetUsersLots(this.state.author, (res) => {
                this.setState({lots: res.data});
            })
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                <h3>Your Lots</h3>
                {this.state.lots.map((lots, index) => {
                    return (
                        <div className="col s3" key={index}>
                            <img src={lots.image ? lots.image : "http://via.placeholder.com/150x150"} alt=""/>
                            <p><Link to={`/lot/${lots.id}`}>{lots.title}</Link></p>
                            <p>Price: {lots.startPrice}</p>
                            <button type="button" onClick={this.DeleteLotHandler} value={lots.id}>Delete</button>
                        </div>
                    )
                })}
                </div>
            </div>
        )
    }
}

export default UsersLots;