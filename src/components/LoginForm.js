import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import LoginService from "../services/LoginService";
import UserProfileService from "../services/UserProfileService";

class LoginForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            error: ''
        };

        this.addLoginService = new LoginService();
        this.addUserProfileService = new UserProfileService();
        this.onLoginClick = this.onLoginClick.bind(this);
        this.onHandleUsernameChange = this.onHandleUsernameChange.bind(this);
        this.onHandlePasswordChange = this.onHandlePasswordChange.bind(this);
    }


    onHandleUsernameChange(e) {
        this.setState({
            login: e.target.value
        })
    }

    onHandlePasswordChange(e) {
        this.setState({
            password: e.target.value
        })
    }

    onLoginClick(e) {
        e.preventDefault();
        const {history} = this.props;
        this.addLoginService.LoginUser(this.state, (res) => {
                if (res === 'Enter correct login and/or password') {
                    this.setState({error: res});
                } else {
                    localStorage.setItem("token", res.data[0].jwt_token);
                    localStorage.setItem("id_user", res.data[0].id);
                    localStorage.setItem("username", res.data[0].login);
                    localStorage.setItem("isLogged", true);

                    history.push('/user-profile');
                }
        });
    }

    render() {
        if(localStorage.getItem("isLogged")) {
            return <Redirect to='/' />
        }

        return (
            <form className="col s12">
                <div className="row">
                    <h4 className="center">Log In</h4>
                    {this.state.error ? <h5 className="center error">{this.state.error}</h5> : false}
                </div>
                <div className="row">
                    <div className="input-field col s4 offset-s4">
                        <input
                            id="first_name"
                            type="text"
                            className="validate"
                            value={this.state.login}
                            onChange={this.onHandleUsernameChange}
                        />
                        <label htmlFor="first_name">Login</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s4 offset-s4">
                        <input
                            id="password"
                            type="password"
                            className="validate"
                            value={this.state.password}
                            onChange={this.onHandlePasswordChange}
                        />
                        <label htmlFor="password">Password</label>
                    </div>
                </div>
                <div className="row center">
                    <button
                        className="btn waves-effect waves-light"
                        type="button"
                        name="action"
                        onClick={this.onLoginClick}>
                        Login
                    </button>
                </div>
            </form>
        );
    }
}

export default LoginForm;