import React, {Component} from 'react';
import UserProfileService from "../services/UserProfileService";
import {Link, Redirect} from 'react-router-dom';
import FileUploadService from "../services/FileUploadService";
import UsersLots from './UsersLots';

class UserProfile extends Component {
    constructor(props) {
        super(props);
        this.state = ({userProfile: []});

        this.addUserProfileService = new UserProfileService();
        this.addFileUploadService = new FileUploadService();
        this.onLogoutButtonClick = this.onLogoutButtonClick.bind(this);
        this.onUploadButtonClick = this.onUploadButtonClick.bind(this);
    }

    componentDidMount() {
        let {history} = this.props;
        this.addUserProfileService.CheckToken(localStorage.getItem("token"), (res) => {
            this.addUserProfileService.GetUserProfile(res.data[0].id, (result) => {
                this.setState({ userProfile: result.data });
                history.push('/user-profile');
            });
        })
    }

    onLogoutButtonClick() {
        let {history} = this.props;

        localStorage.clear();
        history.push('/');
    }

    onUploadButtonClick() {
        const data = new FormData(document.getElementById('form'));
        this.addFileUploadService.UploadImage(this.state.userProfile[0].id, data, (result) => {
            console.log(result);
        })
    }

    render() {
        if(localStorage.getItem("token") === 'undefined' && !localStorage.getItem("isLogged")) {
            localStorage.clear();
            return <Redirect to='/' />
        }

        return (
            <div className="container">
                <div className="row">
                    <ul>
                        <li><Link to='/user-profile/add-new-lot'>Add New Lot</Link></li>
                    </ul>
                <h2 className="center">User Profile</h2>
                {this.state.userProfile.map((userProfile, index) => {
                    return (
                        <div key={index} className="center">
                            <img src={userProfile.avatar ? userProfile.avatar : "NO AVATAR"} alt="" id="avatar"/><br/>
                            <form id="form" encType="multipart/form-data">
                                <input type="file" accept=".jpeg, .jpg, .png" name="file" required/>
                                <button type="submit" onClick={this.onUploadButtonClick}>Upload</button>
                            </form>
                            Hello, {userProfile.name}<br/>
                            Your login is {userProfile.login} <br/>
                            And your email is {userProfile.email} <br/>
                            <button onClick={this.onLogoutButtonClick}>Logout</button>
                            <UsersLots author={userProfile.id}/>
                        </div>
                    )
                })}
                </div>
            </div>
        )
    }
}

export default UserProfile;