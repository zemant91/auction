import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import RegisterService from "../services/RegisterService";

class RegisterForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            surname: '',
            login: '',
            email: '',
            password: '',
            error: '',
            message: ''
        };

        this.addRegisterService = new RegisterService();

        this.onHandleFirstNameChange = this.onHandleFirstNameChange.bind(this);
        this.onHandleLastNameChange = this.onHandleLastNameChange.bind(this);
        this.onHandleLoginChange = this.onHandleLoginChange.bind(this);
        this.onHandleEmailChange = this.onHandleEmailChange.bind(this);
        this.onHandlePasswordChange = this.onHandlePasswordChange.bind(this);

        this.onRegisterClick = this.onRegisterClick.bind(this);
    }

    onHandleFirstNameChange(e) {
        this.setState({
            name: e.target.value
        })
    }

    onHandlePasswordChange(e) {
        this.setState({
            password: e.target.value
        })
    }
    onHandleLoginChange(e) {
        this.setState({
            login: e.target.value
        })
    }

    onHandleLastNameChange(e) {
        this.setState({
            surname: e.target.value
        })
    }

    onHandleEmailChange(e) {
        this.setState({
            email: e.target.value
        })
    }

    onRegisterClick() {
        this.addRegisterService.RegisterUser(this.state, (res) => {
            if (res === 'User with this login is exist') {
                this.setState({error: res});
                this.setState({message: null});
            } else {
                this.setState({message: res});
                this.setState({error: null});
            }
        });
    }


    render() {
        if(localStorage.getItem("isLogged")) {
            return <Redirect to='/' />
        }

        return (
            <form className="col s12">
                <div className="row">
                    <h3 className="center">Registeration</h3>
                    {this.state.error ? <h5 className="center error">{this.state.error}</h5> : false}
                    {this.state.message ? <h5 className="center success">{this.state.message}</h5> : false}
                </div>
                <div className="row">
                    <div className="input-field col s3 offset-s3">
                        <input
                            id="name"
                            type="text"
                            className="validate"
                            value={this.state.name}
                            onChange={this.onHandleFirstNameChange}
                            required/>
                        <label htmlFor="first_name">First Name</label>
                    </div>
                    <div className="input-field col s3">
                        <input
                            id="surname"
                            type="text"
                            className="validate"
                            value={this.state.surname}
                            onChange={this.onHandleLastNameChange}
                            required/>
                        <label htmlFor="last_name">Last Name</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s4 offset-s4">
                        <input
                            id="login"
                            type="text"
                            className="validate"
                            value={this.state.login}
                            onChange={this.onHandleLoginChange}
                            required/>
                        <label htmlFor="last_name">Login</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s4 offset-s4">
                        <input
                            id="email"
                            type="email"
                            className="validate"
                            value={this.state.email}
                            onChange={this.onHandleEmailChange}
                            required/>
                        <label htmlFor="email">Email</label>
                    </div>
                </div>
                <div className="row">
                    <div className="input-field col s4 offset-s4">
                        <input
                            id="password"
                            type="password"
                            className="validate"
                            value={this.state.password}
                            onChange={this.onHandlePasswordChange}
                            required/>
                        <label htmlFor="password">Password</label>
                    </div>
                </div>
                <div className="row center">
                    <button
                        className="btn waves-effect waves-light"
                        type="button"
                        name="action"
                        onClick={this.onRegisterClick}>
                        Register
                    </button>
                </div>
            </form>
        )
    }
}

export default RegisterForm;