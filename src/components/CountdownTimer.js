import React, {Component} from 'react';

class CountdownTimer extends Component {
    constructor(props) {
        super(props);
        this.initializeClock = this.initializeClock.bind(this);
    }

    componentDidMount() {
        this.initializeClock('clockdiv', this.props.end);  
    }


    initializeClock(id, endtime) {
        const clock = document.getElementById(id);
        const daysSpan = clock.querySelector('.days');
        const hoursSpan = clock.querySelector('.hours');
        const minutesSpan = clock.querySelector('.minutes');
        const secondsSpan = clock.querySelector('.seconds');

        function getTimeRemaining(endtime) {
            const t = Date.parse(endtime) - Date.parse(new Date());
            const seconds = Math.floor((t / 1000) % 60);
            const minutes = Math.floor((t / 1000 / 60) % 60);
            const hours = Math.floor((t / (1000 * 60 * 60)) % 24);
            const days = Math.floor(t / (1000 * 60 * 60 * 24));
            return {
                'total': t,
                'days': days,
                'hours': hours,
                'minutes': minutes,
                'seconds': seconds
            };
        }   

        function updateClock() {
            let t = getTimeRemaining(endtime);

            daysSpan.innerHTML = t.days;
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        let timeinterval = setInterval(updateClock, 1000);
    }

    render() {
        return(
        <div id="clockdiv">
          <div>
            <span className="days"></span>
            <div className="smalltext">Days</div>
          </div>
          <div>
            <span className="hours"></span>
            <div className="smalltext">Hours</div>
          </div>
          <div>
            <span className="minutes"></span>
            <div className="smalltext">Minutes</div>
          </div>
          <div>
            <span className="seconds"></span>
            <div className="smalltext">Seconds</div>
          </div>
        </div>
        )
    }
}

export default CountdownTimer;