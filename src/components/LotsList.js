import React, {Component} from 'react';
import LotsService from '../services/LotsService';
import {Link} from 'react-router-dom';
const io = require('socket.io-client');
const socket = io();


class LotsList extends Component {
    constructor(props){
        super(props);
        this.state = {lots: []};

        socket.on('refresh_price', (data) => this.refreshPrices(data))
        this.addLotsService = new LotsService();
        this.sortLots = this.sortLots.bind(this);
    }

    componentDidMount() {
        this.addLotsService.GetAllLots(res => {
            this.setState({lots: res.data});
        })
    }

    sortLots(e) {
        let filterLots = this.state.lots.filter((items) => {
            if (!document.getElementById('search').value) {
                 this.addLotsService.GetAllLots(res => {
                     this.setState({lots: res.data});
                 })
            }
            return items.title.toLowerCase().search(e.target.value.toLowerCase()) !== -1;
        });
        this.setState({lots: filterLots});
    }

    refreshPrices(data) {
        const newLots = this.state.lots.map(lots => {
            if (lots.id == data.id) {
                return {...lots, id: data.id, startPrice: data.price};
            } else {
                return lots;
            }
        })
        this.setState({lots: newLots});
    }

    render() {
        return (
            <main>
                <div className="container">
                    <div className="row center">
                        <div>
                            <input id="search" type="text" onChange={this.sortLots}/>
                        </div>
                        {this.state.lots.map((lots, index) => {
                            return (
                                <div className="lots col s3" key={index}>
                                    <img src={lots.image ? lots.image : "http://via.placeholder.com/150x150"} alt=""/>
                                    <p><Link to={`/lot/${lots.id}`}>{lots.title}</Link></p>
                                    <p>Price: {lots.startPrice}</p>
                                </div>
                            )
                        })}
                    </div>
                </div>
            </main>
        );
    }
}

export default LotsList;

