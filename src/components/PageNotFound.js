import React, {Component} from 'react';

class PageNotFound extends Component {
    render() {
        return (
            <div className="container">
                <h2>OOPS! Page Not Found</h2>
            </div>
        )
    }
}

export default PageNotFound;