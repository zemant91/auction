import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <header>
            <nav>
                <div className="container">
                    <div className="row">
                        <div className="nav-wrapper">
                            <Link to='/'>Logo</Link>
                            <ul id="nav-mobile" className="right hide-on-med-and-down">
                                <li><Link to='/lots'>Lots</Link></li>
                                {localStorage.getItem("isLogged")
                                    ?
                                    <li><Link to='/user-profile'>My Profile</Link></li>
                                    :
                                    <span>
                                        <li><Link to='/login'>Login</Link></li>
                                        <li><Link to='/register'>Register</Link></li>
                                    </span>
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            </header>
        )
    }
}

export default Header;

