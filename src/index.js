import React from 'react';
import ReactDOM from 'react-dom';
import 'materialize-css';
import 'materialize-css/dist/css/materialize.min.css';
import './index.css';
import App from './App';
import {BrowserRouter as Router} from "react-router-dom";
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
    <Router>
        <App/>
    </Router>,
    document.getElementById('root'));
registerServiceWorker();
