import axios from 'axios';

class AddLotService {

    AddNewLot(data, date, callback) {
        axios.post(`http://localhost:3001/api/lots/add-new`, {
            title: data.title,
            description: data.description,
            startPrice: data.startPrice,
            author: data.author,
            end: date
        })
        .then(res => {
            callback(res);
        })
        .catch(err => {
            console.log(err);
        })
    }


}

export default AddLotService;