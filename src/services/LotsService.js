import axios from 'axios';

class LotsService {
    GetAllLots(callback) {
        axios.get('http://localhost:3001/api/lots/get-all')
            .then((res) => {
                callback(res);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    GetOneLot(id, callback) {
        axios.get(`http://localhost:3001/api/lots/get-one/${id}`)
            .then((res) => {
                callback(res);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    GetUsersLots(id, callback) {
        axios.get(`http://localhost:3001/api/lots/get-user-lots/${id}`)
            .then((res) => {
                callback(res);
            })
            .catch((err) => {
                console.log(err)
            })
    }

    DeleteLot(author, id, callback) {
        axios.delete(`http://localhost:3001/api/lots/delete-one/${author}/${id}`)
            .then((res) => {
                callback(res);
            })
            .catch((err) => {
                console.log(err);
            })
    }

    GetUserID(id, callback) {
        axios.get(`http://localhost:3001/api/lots/get-last-user-bid/${id}`)
            .then((res) => {
                callback(res);
            })
            .catch((err) => {
                console.log(err);
            })
    }
}

export default LotsService;