import axios from 'axios';

class RegisterService {
    RegisterUser(data, callback) {
        axios.post('http://localhost:3001/api/users/create-new-user', {
            name: data.name,
            surname: data.surname,
            email: data.email,
            login: data.login,
            password: data.password
        })
            .then((res) => {
                console.log(res.data.message);
                callback(res.data.message);
            })
            .catch((err) => {
                callback(err);
            })
    }
}

export default RegisterService;