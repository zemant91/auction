import axios from 'axios';

class UserProfileService {
    GetUserProfile(id, callback) {
        axios.get(`http://localhost:3001/api/users/get-user/${id}`)
            .then(res => {
                callback(res);
            })
            .catch(err => {
                console.log(err);
            })
    }

    CheckToken(token, callback) {
        axios.get(`http://localhost:3001/api/users/check-token/${token}`)
            .then(res => {
                callback(res);
            })
            .catch(err => {
                console.log(err);
            })
    }
}

export default UserProfileService;