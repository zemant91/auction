import axios from 'axios';

class FileUploadService {
    UploadImage(id, data, callback) {
        axios.put(`http://localhost:3001/api/files/avatar-upload/${id}`, data)
            .then(res => {
                callback(res);
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export default FileUploadService;