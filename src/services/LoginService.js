import axios from 'axios';

class LoginService {
    LoginUser(data, callback) {
        axios.post('http://localhost:3001/api/users/login-user', {
            login: data.login,
            password: data.password
        })
        .then((res) => {
            callback(res);
        })
        .catch((err) => {
            callback(err.response.data.message);
        })
    }
}

export default LoginService;